const ipc = require('electron').ipcRenderer
const fs = require('fs')
const howler_script = document.getElementById('script_howler')

const bodyid = document.getElementById('bodyid')

const img_button_open_directory = document.getElementById('img_button_open_directory')
const img_button_play_pause = document.getElementById('img_button_play_pause')
const select_song_list = document.getElementById('select_song_list')
const slider_song_position = document.getElementById('slider_song_position')
const label_song_position = document.getElementById('label_song_position')
const slider_volume = document.getElementById('slider_volume')
const img_button_volume = document.getElementById('img_button_volume')
const label_volume = document.getElementById('label_volume')
const checkbox_autoplay = document.getElementById('checkbox_autoplay')
const checkbox_loop = document.getElementById('checkbox_loop')

var sound

var PLAYING = false
var SOUND_LOADED = false
var SELECTED_SONG_INDEX = -1
var CURRENT_SONG_INDEX = -1
var CURRENT_SONG_POSITION = 1
var SLIDER_POSITION_MOUSE_DOWN = false
var SLIDER_VOLUME_MOUSE_DOWN = false
var MUTE = false
var SET_VOLUME = 0
var CONTROLS_ALLOWED = false

var MUSIC_DIR = ""
var MUSIC_ARRAY = []

//--------------- EVENTS ---------------

img_button_open_directory.addEventListener('click', function (events) { ipc.send('r_message_open_dialog') })
img_button_open_directory.addEventListener('mousedown', mousedown_img_button_open_directory)
img_button_open_directory.addEventListener('mouseup', mouseup_img_button_open_directory)
img_button_open_directory.addEventListener('mouseenter', mouseenter_img_button_open_directory)
img_button_open_directory.addEventListener('mouseleave', mouseleave_img_button_open_directory)
img_button_open_directory.ondragstart = function() {return false}

img_button_play_pause.addEventListener('click', handle_play_pause)
img_button_play_pause.addEventListener('mousedown', mousedown_img_button_play_pause)
img_button_play_pause.addEventListener('mouseup', mouseup_img_button_play_pause)
img_button_play_pause.addEventListener('mouseenter', mouseenter_img_button_play_pause)
img_button_play_pause.addEventListener('mouseleave', mouseleave_img_button_play_pause)
img_button_play_pause.ondragstart = function() {return false}

img_button_volume.addEventListener('click', handle_mute)
img_button_volume.addEventListener('mousedown', mousedown_img_button_volume)
img_button_volume.addEventListener('mouseleave', mouseup_img_button_volume)
img_button_volume.addEventListener('mouseenter', mouseenter_img_button_volume)
img_button_volume.addEventListener('mouseup', mouseleave_img_button_volume)
img_button_volume.ondragstart = function() {return false}

select_song_list.addEventListener('click', handle_selection)
slider_song_position.addEventListener('mousedown', handle_selection_mousedown)
slider_song_position.addEventListener('mouseup', handle_selection_mouseup)
slider_volume.addEventListener('mousedown', handle_volume_mousedown)
slider_volume.addEventListener('mouseup', handle_volume_mouseup)

checkbox_loop.addEventListener('click', handle_checkbox_loop_click)
checkbox_autoplay.addEventListener('click', handle_checkbox_autoplay_click)

var timer = setInterval(() => { handle_timer() }, 100)

document.addEventListener("mouseleave", handle_mouseleave)
document.addEventListener("mouseup", handle_mouseup)

ipc.on('m_message_path', function (event, path) {
    if (path != "") {
        MUSIC_DIR = path[0]
        handle_load_list()
    }
})

var start_option = document.createElement('option')
start_option.value = "none"
start_option.innerHTML = ""
document.getElementById('select_song_list').appendChild(start_option)
document.getElementById('select_song_list').appendChild(start_option)
document.getElementById('select_song_list').appendChild(start_option)
document.getElementById('select_song_list').appendChild(start_option)
document.getElementById('select_song_list').appendChild(start_option)
document.getElementById('select_song_list').appendChild(start_option)
document.getElementById('select_song_list').appendChild(start_option)
document.getElementById('select_song_list').appendChild(start_option)
document.getElementById('select_song_list').appendChild(start_option)

//--------------- EVENT FUNCTIONS ---------------

function handle_load_list() {
    MUSIC_ARRAY = []
    CONTROLS_ALLOWED = false
    if (SOUND_LOADED == true) {
        SOUND_LOADED = false
        PLAYING = false
        sound.stop()
        img_button_play_pause.src = "assets/button_play.png"
        slider_song_position.value = 0
        var current = translate_time(parseInt(0))
        var dur = translate_time(parseInt(0))
        label_song_position.innerHTML = current + " / " + dur

    }

    document.getElementById('select_song_list').innerHTML = ''
    fs.readdir(MUSIC_DIR, (err, files) => {
        files.forEach(file => {

            //mp3 mpeg opus ogg oga wav aac caf m4a mp4 weba webm dolby flac
            if (file.endsWith(".mp3") || file.endsWith(".mpeg") || file.endsWith(".opus") || file.endsWith(".ogg") || file.endsWith(".oga") || file.endsWith(".wav") || file.endsWith(".aac") || file.endsWith(".caf") || file.endsWith(".m4a") || file.endsWith(".mp4") || file.endsWith(".weba") || file.endsWith(".webm") || file.endsWith(".dolby") || file.endsWith(".flac")) {
                MUSIC_ARRAY.push(file)
                var new_option = document.createElement('option')
                new_option.value = file
                new_option.innerHTML = file
                document.getElementById('select_song_list').appendChild(new_option)
            }
        })
        select_song_list.selectedIndex = 0
        if (MUSIC_ARRAY.length > 0) {
            CONTROLS_ALLOWED = true
        }
    })
}

function handle_play_pause() {
    if (CONTROLS_ALLOWED == true) {
        if (PLAYING == true && CURRENT_SONG_INDEX == SELECTED_SONG_INDEX) {
            PLAYING = false
            sound.pause()
        }

        else {
            if (SOUND_LOADED == false) {
                var encoded_song = MUSIC_DIR + "/" + encodeURIComponent(select_song_list.value)
                sound = new Howl({ src: [encoded_song], html5: true })
                SOUND_LOADED = true
                SELECTED_SONG_INDEX = select_song_list.selectedIndex
                CURRENT_SONG_INDEX = select_song_list.selectedIndex
            }
            else {
                if (CURRENT_SONG_INDEX != SELECTED_SONG_INDEX) {
                    sound.stop()
                    var encoded_song = MUSIC_DIR + "/" + encodeURIComponent(select_song_list.value)
                    sound = new Howl({ src: [encoded_song], html5: true })
                    SOUND_LOADED = true
                    SELECTED_SONG_INDEX = select_song_list.selectedIndex
                    CURRENT_SONG_INDEX = select_song_list.selectedIndex
                }
            }

            sound.volume(slider_volume.value / 100)
            sound.play()
            PLAYING = true
        }
    }
    handle_mouseup()
}

function handle_selection() { 
    SELECTED_SONG_INDEX = select_song_list.selectedIndex

    if (SELECTED_SONG_INDEX == CURRENT_SONG_INDEX) {
        img_button_play_pause.src = "assets/button_pause.png"
    }
    else {
        img_button_play_pause.src = "assets/button_play.png"
    }

    handle_mouseup()
}

function handle_selection_mousedown() {
    if (CONTROLS_ALLOWED == true) {
        SLIDER_POSITION_MOUSE_DOWN = true
    }
    else {
        slider_song_position.value = 0
    }
}

function handle_selection_mouseup() {
    if (CONTROLS_ALLOWED == true) {
        SLIDER_POSITION_MOUSE_DOWN = false
        if (SOUND_LOADED == true) {
            sound.seek(slider_song_position.value)
        }
    }
    else {
        slider_song_position.value = 0
    }
}

function handle_timer() {
    if (SOUND_LOADED == true) {
        CURRENT_SONG_POSITION = parseInt(sound.seek())
        slider_song_position.max = parseInt(sound.duration()).toString()
        if (SLIDER_POSITION_MOUSE_DOWN == false) { 
            slider_song_position.value = CURRENT_SONG_POSITION.toString()
        }
        var current = translate_time(parseInt(CURRENT_SONG_POSITION))
        var dur = translate_time(parseInt(sound.duration()))
        label_song_position.innerHTML = current + " / " + dur
    }

    if (PLAYING == true && sound.state() == "loaded") {
        if (sound.playing() == false) {
            handle_finished()
        }
    }
}

function handle_finished() {
    if (SOUND_LOADED == true) {
        if (checkbox_autoplay.checked == true) {
            if (CURRENT_SONG_INDEX < MUSIC_ARRAY.length - 1) {
                CURRENT_SONG_INDEX += 1           
            }

            if (checkbox_loop.checked == true) {
                if (CURRENT_SONG_INDEX == MUSIC_ARRAY.length - 1) {
                    CURRENT_SONG_INDEX = 0
                }
            }
            
            var encoded_song = MUSIC_DIR + "/" + encodeURIComponent(MUSIC_ARRAY[CURRENT_SONG_INDEX])
            sound = new Howl({ src: [encoded_song], html5: true })
            SELECTED_SONG_INDEX = CURRENT_SONG_INDEX
            select_song_list.selectedIndex = SELECTED_SONG_INDEX
            sound.play()
            //CODE HERE FOR CHANGING THE SELECTION IN SELECT SONG LIST
        }
        else {
            SOUND_LOADED = false
            PLAYING = false
            sound.stop()
            img_button_play_pause.src = "assets/button_play.png"
            slider_song_position.value = 0
            var current = translate_time(parseInt(0))
            var dur = translate_time(parseInt(0))
            label_song_position.innerHTML = current + " / " + dur
        }
    }
}

function handle_checkbox_loop_click() {
    if (checkbox_autoplay.checked == false) {
        checkbox_loop.checked = false
    }
}

function handle_checkbox_autoplay_click() {
    if (checkbox_autoplay.checked == false) {
        checkbox_loop.checked = false
    }
}

//--------------- VOLUME FUNCTIONS ---------------

function handle_volume_mousedown () {
    SLIDER_VOLUME_MOUSE_DOWN = true
    //if (SOUND_LOADED == true) {
    //    sound.volume(slider_volume.value / 100)
    //}
}

function handle_volume_mouseup () {
    SLIDER_VOLUME_MOUSE_DOWN = false
    label_volume.innerHTML = slider_volume.value
    if (SOUND_LOADED == true) {
        sound.volume(slider_volume.value / 100)
        label_volume.innerHTML = slider_volume.value
        MUTE = false
    }
}

function handle_mute() {
    if (SOUND_LOADED == true) {
        if (MUTE == false) {
            MUTE = true
            SET_VOLUME = slider_volume.value
            slider_volume.value = 0
            label_volume.innerHTML = slider_volume.value
            sound.volume(0)
        }
        else {
            MUTE = false
            if (slider_volume.value == 0) {
                slider_volume.value = SET_VOLUME
            }
 
            label_volume.innerHTML = slider_volume.value
            sound.volume(slider_volume.value / 100)
        }
    }
    else {
        if (MUTE == false) {
            MUTE = true
            SET_VOLUME = slider_volume.value
            slider_volume.value = 0
            label_volume.innerHTML = slider_volume.value
        }
        else {
            MUTE = false
            if (slider_volume.value == 0) {
                slider_volume.value = SET_VOLUME
            }
            label_volume.innerHTML = slider_volume.value
        }
    }
    handle_mouseup()
}


//total = 8397
function translate_time(total) {
    
    var hours = 0
    var string_hours = ""
    var minutes = 0
    var string_minutes = ""
    var string_left_over_seconds = ""

    if (total > 3600) {
        left_over_seconds = parseInt(total) % 60
        left_over_minutes = parseInt(total) % 3600
        minutes = parseInt(left_over_minutes / 60)
        hourminutes = (total - left_over_minutes) / 60
        hours = hourminutes / 60
    }
    else {
        left_over_seconds = parseInt(total) % 60
        minutes = parseInt((total - left_over_seconds) / 60)
    }
    
    if (hours < 10) { string_hours = "0" + hours.toString() }
    if (hours >= 10) { string_hours = hours.toString() }
    if (minutes < 10) { string_minutes = "0" + minutes.toString() }
    if (minutes >= 10) { string_minutes = minutes.toString() }
    if (left_over_seconds < 10) { string_left_over_seconds = "0" + left_over_seconds.toString() }
    if (left_over_seconds >= 10) { string_left_over_seconds = left_over_seconds.toString() }
    
    return string_hours + ":" + string_minutes + ":" + string_left_over_seconds
    //return hours.toString() + ":" + minutes.toString() + ":" + left_over_seconds.toString()
}

//--------------- BUTTON ANIMATIONS MOUSEDOWN AND MOUSEUP ---------------

function mousedown_img_button_open_directory() {
    img_button_open_directory.src = "assets/button_open_mousedown_64.png"
}

function mouseup_img_button_open_directory() {
    img_button_open_directory.src = "assets/button_open_64.png"
}

function mouseenter_img_button_open_directory() {
    img_button_open_directory.src = "assets/button_open_64_hover.png"
}

function mouseleave_img_button_open_directory() {
    img_button_open_directory.src = "assets/button_open_64.png"
}

//---------------

function mousedown_img_button_play_pause() {
    if (CONTROLS_ALLOWED == true) {
        if (PLAYING == true && SELECTED_SONG_INDEX == CURRENT_SONG_INDEX) {
            img_button_play_pause.src = "assets/button_pause_mousedown_64.png"
        }
        else {
            img_button_play_pause.src = "assets/button_play_mousedown_64.png"
        }
    }
    else {
        img_button_play_pause.src = "assets/button_play_64.png"
    }
    
}

function mouseup_img_button_play_pause() {
    if (CONTROLS_ALLOWED == true) {
        if (PLAYING == true && SELECTED_SONG_INDEX == CURRENT_SONG_INDEX) {
            img_button_play_pause.src = "assets/button_pause_64.png"
        }
        else {
            img_button_play_pause.src = "assets/button_play_64.png"
        }
    }
    else {
        img_button_play_pause.src = "assets/button_play_64.png"
    }
}

function mouseenter_img_button_play_pause() {
    if (CONTROLS_ALLOWED == true) {
        if (PLAYING == true && SELECTED_SONG_INDEX == CURRENT_SONG_INDEX) {
            img_button_play_pause.src = "assets/button_pause_64_hover.png"
        }
        else {
            img_button_play_pause.src = "assets/button_play_64_hover.png"
        }
    }
    else {
        img_button_play_pause.src = "assets/button_play_64_hover.png"
    }
}

function mouseleave_img_button_play_pause() {
    if (CONTROLS_ALLOWED == true) {
        if (PLAYING == true && SELECTED_SONG_INDEX == CURRENT_SONG_INDEX) {
            img_button_play_pause.src = "assets/button_pause_64.png"
        }
        else {
            img_button_play_pause.src = "assets/button_play_64.png"
        }
    }
    else {
        img_button_play_pause.src = "assets/button_play_64.png"
    }
}

//---------------

function mousedown_img_button_volume() {
    if (MUTE == true) {
        img_button_volume.src = "assets/button_mute_mousedown_64.png"
    }
    else {
        img_button_volume.src = "assets/button_sound_mousedown_64.png"
    }
}

function mouseup_img_button_volume() {
    if (MUTE == true) {
        img_button_volume.src = "assets/button_mute_64.png"
    }
    else {
        img_button_volume.src = "assets/button_sound_64.png"
    }
}

function mouseenter_img_button_volume() {
    if (MUTE == true) {
        img_button_volume.src = "assets/button_mute_64_hover.png"
    }
    else {
        img_button_volume.src = "assets/button_sound_64_hover.png"
    }
}

function mouseleave_img_button_volume() {
    if (MUTE == true) {
        img_button_volume.src = "assets/button_mute_64.png"
    }
    else {
        img_button_volume.src = "assets/button_sound_64.png"
    }
}

function handle_mouseleave() {
    mouseup_img_button_open_directory()
    mouseup_img_button_volume()
    mouseup_img_button_play_pause()
}

function handle_mouseup() {
    mouseup_img_button_open_directory()
    mouseup_img_button_volume()
    mouseup_img_button_play_pause()
}