// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')
const ipc = require('electron').ipcMain
const dialog = require('electron').dialog
const path = require('path')

function createWindow () {
  const mainWindow = new BrowserWindow({
    width: 470,
    height: 340,
    autoHideMenuBar: true,
    icon: path.join(__dirname, '/assets/512x512.png'),
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      enableRemoteModule: false,
      contextIsolation: false,
    }
  })
  mainWindow.loadFile('index.html')
  //mainWindow.removeMenu()
  //mainWindow.setResizable(false)
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

ipc.on('r_message_open_dialog', async function (e) {
  var get_message = await open_dialog(e)
  e.sender.send('m_message_path', get_message)
}) 

async function open_dialog(e) {
  var dvalue = dialog.showOpenDialog( { properties: ['openFile','openDirectory'] })
  return_message = (await dvalue).filePaths
  return(return_message)
}