# slmp

Simple Local Music Player

## What is it?

My use case for a music player is a little niche but hopefully it appeals to others as well. If so, please feel free to use or modifiy this player to your needs.

I am still someone that downloads music and organizes them in to directories. I have been using players like MOC in the terminal that almost met my needs. There were only minor annoyances like being able to skip where I wanted to in long playlist files. I also wanted an opportunity to start learning how to use electron.

## How to build.

1. Clone the repo
2. npm install
3. npm install electron-builder
4. npm run dist

Currently this only builds for Linux as an appimage but you can modify the package.json to suit your needs for other builds.

## Other / License

I am still in the proecss of updating the misc files as I am new. Currently even though it does not state it -- this is MIT licensed. Do with it as you please.

```

